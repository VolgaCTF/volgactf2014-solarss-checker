<?php
require('vendor/autoload.php');
include 'functions.php';

use Femida\Checker;
use Femida\Result;


	class SolarssChecker extends Checker{
		protected $ip;
		protected $port;
		protected $flag;
		protected $flag_id;

		protected $login;
		protected $password;


		protected $cookie;
		protected $channels;

		protected $prepath = '';

		public function construct($endpoint, $flag_id, $flag){
			list($this->ip, $this->port) = explode(':', $endpoint);
			$this->flag_id = $flag_id;
			$this->flag = $flag;
			$this->login = substr(sha1($flag_id),0,20).'@flager.com';
			$this->password = substr(sha1($flag_id),20);
			$this->cookie = "";
		}

		protected $categories = array(
			"World",
			"Local",
			"Education",
			"Science",
			"Technology",
			"Kids",
			"Sports",
			"Entertainment");

		private function url($url){
			return 'http://'.$this->ip.':'.$this->port.$this->prepath.$url;
		}


		private function curlRequest($url, $params, &$rheaders, &$rbody){
			$ch = curl_init();

			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.137 Safari/537.36");
			curl_setopt($ch, CURLOPT_COOKIE, $this->cookie);
			if( $params != NULL){
				curl_setopt($ch, CURLOPT_POST, 1);
				curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
			}
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			@curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
			curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
			curl_setopt($ch, CURLOPT_HEADER, true);
			
			$data = curl_exec($ch);
			
			$info = curl_getinfo($ch);
			$rbody = $info["size_download"] ? substr($data, $info["header_size"], $info["size_download"]) : "";
		
			$rheaders = substr($data, 0, $info["header_size"]);
			curl_close ($ch);

			return $info['http_code'];
		}

		private function setCookie($headers){
			$arr = explode("\n",$headers);
			foreach ($arr as $line) {
				if(preg_match('#PHPSESSID=\w*;#',  $line, $matches)){
					$this->cookie = $matches[0];
					return TRUE;
				}
			}
			return FALSE;
		}

		public function isUp(){
			$code = $this->curlRequest( $this->url('/'), 0, $headers, $body );
			return ($code == 404 || $code == 0) ? FALSE : TRUE;
		}

		private function register(){
			$params = "login=".urlencode($this->login)."&password=".urlencode($this->password);
			$this->curlRequest( $this->url('/register/'), $params, $headers, $body );
			
			return ($this->setCookie($headers) && !preg_match('#User exists#si', $body)); 
		}

		private function login(){
			$params = "login=".urlencode($this->login)."&password=".urlencode($this->password);
			$this->curlRequest( $this->url('/login/'), $params, $headers, $body );
			
			return ($this->setCookie($headers) && !preg_match('#user not found#si', $body)); 
		}


		private function findChannels($text){
			preg_match_all('#<a href="[^"]*?channel/\?id=(\d+)"#si', $text, $matches);
			return $matches[1];
		}

		private function createChannels(){
			$categories = [];
			for($i = 0; $i < 3; $i++){
				$categories[] = $this->categories[mt_rand(0, count($this->categories) - 1 )];
			}
			$categories[] = "Private"; 

			foreach ($categories as $category) {
				$name = "{$category}_".substr(base64_encode($this->login),0,20);
				$params = "action=createChannel&channelName=$name&category=$category";
				$this->curlRequest( $this->url('/userPage/'), $params, $headers, $body );
			}

			$channels = $this->findChannels($body);
			if(empty($channels))
				return FALSE;

			foreach ($categories as $num => $category) {
				$this->channels[$category] = $channels[$num];
			};

			return TRUE;
		}


		private function uploadFlag(){
			$rss = rssList();

			$articles = [];
			foreach ($this->channels as $category => $channelId){
				if($category == 'Private')
					continue;

				$r = 0;
				while( $r == 0){
					$link = $rss[$category][mt_rand(0, count($rss[$category]) - 1 )];
					$rawFeed = file_get_contents($link);
					$this->logger->addInfo("Visiting $link");
					$xml = new SimpleXmlElement($rawFeed);
					$r = count($xml->channel->item);
				}
					
				$item = $xml->channel->item[mt_rand(0, $r - 1 )];

				$article = array();
				$article['title'] = $item->title;
				$article['description'] = (string) trim($item->description);
				$article['link'] = $item->link;

				$content = $item->children('http://purl.org/rss/1.0/modules/content/');
				$article['content'] = (string)trim($content->encoded);
		 		if(empty($content))
		 			$article['content'] = "Read more <a href='{$article['link']}' target='_blank'>using this link</a>";
		 		$article['channelId'] = $channelId;
		 		$articles[$category] = $article;

			}

			$articles["Private"] = array(
					"title" => "Something secret",
					"description" => $this->flag,
					"content" => "Yep, flag is on this page",
					"channelId" => $this->channels["Private"],
					);
			foreach ($articles as $article) {
				$params = 	"title=".urlencode($article['title']).
							"&channelId=".urlencode($article['channelId']).
							"&description=".urlencode($article['description']).
							"&content=".urlencode($article['content'])."";
				$this->curlRequest( $this->url('/add/'), $params, $headers, $body );
			}

			return $this->checkFlag();

		}


		private function checkFlag(){
			$this->curlRequest( $this->url('/category/?category=Private'), null, $headers, $body );

			if(strpos($body, $this->flag) !== FALSE)
				return TRUE;
			return FALSE;
		
		}


		protected function _push($endpoint, $flagID, $flag)
    	{
        	$this->construct($endpoint . ':8010', $flagID, $flag);
			//isUp ?
			if( !$this->isUp())
				return array(Result::DOWN, $flagID);

			//register
			if( !$this->register())
				return array(Result::MUMBLE, $flagID);

			//create channels
			if( !$this->createChannels())
				return array(Result::MUMBLE, $flagID);

			//upload flag
			if( !$this->uploadFlag())
				return array(Result::MUMBLE, $flagID);

			return array(Result::OK,$flagID);

		}

		protected function _pull($endpoint, $flagID, $flag)
    	{ 	//isUp ?
		$this->construct($endpoint . ':8010', $flagID, $flag);
			if( !$this->isUp())
				return Result::DOWN;

			//login using OTP
			if( !$this->login())
				return Result::MUMBLE;

			//check flag
			if( !$this->checkFlag())
				return Result::CORRUPT;

			return Result::OK;
		}

	}

$checker = new SolarssChecker();
$checker->run();

	//`rm -f ../service/*.db`;
/*
	$t = time();
	$flag_id = time();
	$flag = time();
	$checker = new Checker('192.168.1.176:80',$flag_id , $flag.'abcdef0123456789abcdef1=');
	echo "push -> " . $checker->push();
	echo "<br>\n";
	$checker = new Checker('192.168.1.176:80',$flag_id , $flag.'abcdef0123456789abcdef1=');
	echo "pull -> " .$checker->pull();
	echo "<br>\n";
	echo "executed in ".(time() - $t)." seconds\n";*/
